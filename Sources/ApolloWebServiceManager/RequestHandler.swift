//
//  RequestHandler.swift
//
//  Created by Seyed Mojtaba Hosseini Zeidabadi on 6/30/20.
//  Copyright © 2020 Chenzook. All rights reserved.
//
//  StackOverflow: https://stackoverflow.com/story/mojtabahosseini
//  Linkedin: https://linkedin.com/in/MojtabaHosseini
//  GitHub: https://github.com/MojtabaHs
//  Web: https://chenzook.com
//

import Foundation
import Apollo

public protocol URLRequestValidator {
    func manager(_ manager:  ApolloWebServiceManager, shouldSend request: URLRequest) -> Bool
}

public protocol URLRequestAdapter {
    func manager(_ manager:  ApolloWebServiceManager, willSend request: inout URLRequest)
}

// MARK: Methods which will be called prior to a request being sent to the server.
extension  ApolloWebServiceManager {
    /// Called when a request is about to send, to validate that it should be sent.
    /// Good for early-exiting if your user is not logged in, for example.
    ///
    /// - Parameters:
    ///   - networkTransport: The network transport which wants to send a request
    ///   - request: The request, BEFORE it has been modified by `willSend`
    /// - Returns: True if the request should proceed, false if not.
    public func networkTransport(_ networkTransport: HTTPNetworkTransport,
                                 shouldSend request: URLRequest) -> Bool {
        for validator in urlRequestValidators { guard validator.manager(self, shouldSend: request) else { return false } }
        return true
    }

    /// Called when a request is about to send. Allows last minute modification of any properties on the request,
    ///
    /// - Parameters:
    ///   - networkTransport: The network transport which is about to send a request
    ///   - request: The request, as an `inout` variable for modification
    public func networkTransport(_ networkTransport: HTTPNetworkTransport,
                                 willSend request: inout URLRequest) {
        for adapter in urlRequestAdapters { adapter.manager(self, willSend: &request) }
    }
}
