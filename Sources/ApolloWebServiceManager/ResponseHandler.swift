//
//  ResponseHandler.swift
//
//  Created by Seyed Mojtaba Hosseini Zeidabadi on 6/30/20.
//  Copyright © 2020 Chenzook. All rights reserved.
//
//  StackOverflow: https://stackoverflow.com/story/mojtabahosseini
//  Linkedin: https://linkedin.com/in/MojtabaHosseini
//  GitHub: https://github.com/MojtabaHs
//  Web: https://chenzook.com
//

import Foundation
import Apollo

public protocol URLResponseObserver {
    func manager(_ manager:  ApolloWebServiceManager,
                 didCompleteRawTaskForRequest request: URLRequest,
                 withData data: Data?,
                 response: URLResponse?,
                 error: Error?)
}

public enum RetryContinueAction {
    case retry
    case fail(Error)
}

public protocol URLRequestRetrier {
    func processToRetryWhenManager(_ manager:  ApolloWebServiceManager,
                                   onReceivedError error: Error,
                                   for request: URLRequest,
                                   response: URLResponse?,
                                   continueHandler: @escaping (_ action: RetryContinueAction) -> Void) -> Bool
}

public protocol GraphQLErrorObserver {
    func processToRetryWhenManager(_ manager:  ApolloWebServiceManager,
                                   receivedGraphQLErrors errors: [Error],
                                   retryHandler: @escaping (_ shouldRetry: Bool) -> Void) -> Bool
}

// MARK: Methods which will be called after some kind of response has been received to a `URLSessionTask`.
extension  ApolloWebServiceManager {
    /// A callback to allow hooking in URL session responses for things like logging and examining headers.
    /// NOTE: This will call back on whatever thread the URL session calls back on, which is never the main thread. Call `DispatchQueue.main.async` before touching your UI!
    ///
    /// - Parameters:
    ///   - networkTransport: The network transport that completed a task
    ///   - request: The request which was completed by the task
    ///   - data: [optional] Any data received. Passed through from `URLSession`.
    ///   - response: [optional] Any response received. Passed through from `URLSession`.
    ///   - error: [optional] Any error received. Passed through from `URLSession`.
    public func networkTransport(_ networkTransport: HTTPNetworkTransport,
                                 didCompleteRawTaskForRequest request: URLRequest,
                                 withData data: Data?,
                                 response: URLResponse?,
                                 error: Error?) {
        for observer in urlResponseObservers {
            observer.manager(self,
                             didCompleteRawTaskForRequest: request,
                             withData: data,
                             response: response,
                             error: error)
        }
    }

    // MARK: - Methods which will be called if an error is received at the network level.

    /// Called when an error has been received after a request has been sent to the server to see if an operation should be retried or not.
    /// NOTE: Don't just call the `continueHandler` with `.retry` all the time, or you can potentially wind up in an infinite loop of errors
    ///
    /// - Parameters:
    ///   - networkTransport: The network transport which received the error
    ///   - error: The received error
    ///   - request: The URLRequest which generated the error
    ///   - response: [Optional] Any response received when the error was generated
    ///   - continueHandler: A closure indicating whether the operation should be retried. Asynchronous to allow for re-authentication or other async operations to complete.
    public func networkTransport(_ networkTransport: HTTPNetworkTransport,
                                 receivedError error: Error,
                                 for request: URLRequest,
                                 response: URLResponse?,
                                 continueHandler: @escaping (_ action: HTTPNetworkTransport.ContinueAction) -> Void) {

        func continueHandlerForAction(_ action: RetryContinueAction) {
            switch action {
            case .retry: continueHandler(.retry)
            case .fail(let error): continueHandler(.fail(error))
            }
        }

        func retry(retriers: Array<URLRequestRetrier>.SubSequence) {
            guard let retrier = retriers.first else { return debugPrint("No more retrier") }

            guard retrier.processToRetryWhenManager(self,
                            onReceivedError: error,
                            for: request,
                            response: response,
                            continueHandler: continueHandlerForAction) else { return }
            retry(retriers: retriers.dropFirst())
        }
    }

    // MARK: - Methods which will be called after some kind of response has been received and it contains GraphQLErrors.


    /// Called when response contains one or more GraphQL errors.
    ///
    /// NOTE: The mere presence of a GraphQL error does not necessarily mean a request failed!
    ///       GraphQL is design to allow partial success/failures to return, so make sure
    ///       you're validating the *type* of error you're getting in this before deciding whether to retry or not.
    ///
    /// ALSO NOTE: Don't just call the `retryHandler` with `true` all the time, or you can
    ///            potentially wind up in an infinite loop of errors
    ///
    /// - Parameters:
    ///   - networkTransport: The network transport which received the error
    ///   - errors: The received GraphQL errors
    ///   - retryHandler: A closure indicating whether the operation should be retried. Asynchronous to allow for re-authentication or other async operations to complete.
    public func networkTransport(_ networkTransport: HTTPNetworkTransport,
                                 receivedGraphQLErrors errors: [GraphQLError],
                                 retryHandler: @escaping (_ shouldRetry: Bool) -> Void) {

        func retry(observers: Array<GraphQLErrorObserver>.SubSequence) {
            guard let observer = observers.first else { return debugPrint("No more error observer") }

            guard observer.processToRetryWhenManager(self,
                                                     receivedGraphQLErrors: errors,
                                                     retryHandler: retryHandler) else { return }
            retry(observers: observers.dropFirst())
        }

    }
}
