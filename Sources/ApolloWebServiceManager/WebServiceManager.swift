//
//  ApolloWebServiceManager.swift
//
//  Created by Seyed Mojtaba Hosseini Zeidabadi on 6/29/20.
//  Copyright © 2020 Chenzook. All rights reserved.
//
//  StackOverflow: https://stackoverflow.com/story/mojtabahosseini
//  Linkedin: https://linkedin.com/in/MojtabaHosseini
//  GitHub: https://github.com/MojtabaHs
//  Web: https://chenzook.com
//

import Foundation
import Apollo

public class  ApolloWebServiceManager {
    init(baseURL: URL) {
        self.baseURL = baseURL
        self.httpNetworkTransport = HTTPNetworkTransport(url: baseURL)
        defer { self.httpNetworkTransport.delegate = self }

        self.apollo = ApolloClient(networkTransport: httpNetworkTransport)
    }

    public let baseURL: URL
    private let apollo: ApolloClient
    private let httpNetworkTransport: HTTPNetworkTransport

    /// Executed when a request is about to send, to validate that it should be sent.
    /// Good for early-exiting if your user is not logged in, for example.
    public var urlRequestValidators = [URLRequestValidator]()

    /// Executed when a request is about to send. Allows last minute modification of any properties on the request,
    public var urlRequestAdapters = [URLRequestAdapter]()

    /// Called when an error has been received after a request has been sent to the server to see if an operation should be retried or not.
    /// NOTE: Don't just call the `continueHandler` with `.retry` all the time, or you can potentially wind up in an infinite loop of errors
    /// ALSO NOTE: Return `false` when retrier is about to perform async task.
    public var requestRetriers = [URLRequestRetrier]()

    /// Hooks in URL session responses for things like logging and examining headers.
    /// NOTE: This will call  on whatever thread the URL session calls back on, which is never the main thread. Call `DispatchQueue.main.async` before touching your UI!
    public var urlResponseObservers = [URLResponseObserver]()


    /// Called when response contains one or more GraphQL errors.
    /// NOTE: The mere presence of a GraphQL error does not necessarily mean a request failed!
    ///       GraphQL is design to allow partial success/failures to return, so make sure
    ///       you're validating the *type* of error you're getting in this before deciding whether to retry or not.
    /// ALSO NOTE: Don't just call the `retryHandler` with `true` all the time, or you can
    ///            potentially wind up in an infinite loop of errors
    /// ALSO NOTE: Return `false` when retrier is about to perform async task.
    public var graphQLErrorObservers = [GraphQLErrorObserver]()

    @discardableResult
    func fetch<T: GraphQLQuery>(query: T,
                                cachePolicy: CachePolicy = .fetchIgnoringCacheData,
                                context: UnsafeMutableRawPointer? = nil,
                                queue: DispatchQueue = .global(qos: .background),
                                resultHandler: ((Result<GraphQLResult<T.Data>, Error>) -> Void)?) -> Cancellable {
        apollo.fetch(query: query,
                     cachePolicy: cachePolicy,
                     context: context,
                     queue: queue,
                     resultHandler: resultHandler)
    }

    @discardableResult
    func perform<T: GraphQLMutation>(mutation: T,
                                     context: UnsafeMutableRawPointer? = nil,
                                     queue: DispatchQueue = .global(qos: .background),
                                     resultHandler: ((Result<GraphQLResult<T.Data>, Error>) -> Void)?) -> Cancellable {
        apollo.perform(mutation: mutation,
                       context: context,
                       queue: queue,
                       resultHandler: resultHandler)
    }
}

// MARK: - HTTPNetworkTransportPreflightDelegate

extension  ApolloWebServiceManager: HTTPNetworkTransportPreflightDelegate { }
