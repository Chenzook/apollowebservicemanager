import XCTest

import ApolloWebServiceManagerTests

var tests = [XCTestCaseEntry]()
tests += ApolloWebServiceManagerTests.allTests()
XCTMain(tests)
